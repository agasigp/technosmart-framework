<table id="usergrid" class="easyui-datagrid" 
       style="width:700px;height:300px;" 
       url="<?php echo site_url('sistem/user/get_user_json'); ?>" 
       toolbar="#tbuser" 
       title="User" 
       iconCls="icon-save"  
       rownumbers="true" pagination="true">  

    <thead>  
        <tr>  
            <th field="user_id" width="60" align="center">User ID</th>  
            <th field="user_nama" width="100" align="left">Username</th>  
            <th field="user_real_nama" width="150" align="left">Nama</th>  
            <th field="user_email" width="190" align="left">Email</th>  
            <th field="user_status" width="60" align="center">Status</th>  
            <th field="user_group" width="60" align="center">Group</th>  
        </tr>  
    </thead>  
</table>

<div id="user_dialog" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px" closed="true" buttons="#dlg-buttons">  
    <div class="ftitle">User Information</div>  
    <form id="form" method="post">  
        <div class="fitem">  
            <label>Username:</label>  
            <input name="user_nama" class="easyui-validatebox" required="true">  
        </div>  
        <div class="fitem">  
            <label>Nama:</label>  
            <input name="user_real_nama" class="easyui-validatebox" required="true">  
        </div>  
        <div class="fitem">  
            <label>Password</label>  
            <input name="password" type="password" class="easyui-validatebox" required="true">  
        </div> 
        <div class="fitem">  
            <label>Email:</label>  
            <input name="user_email" class="easyui-validatebox" required="true" validType="email">  
        </div>  
        <div class="fitem">  
            <label>Status:</label>  
            <select id="cc" class="easyui-combobox" name="user_status" style="width:100px;">  
                <option value="Aktif">Aktif</option>  
                <option value="Tidak">Tidak</option>  
            </select> 
        </div>  
        <div class="fitem">  
            <label>Group:</label>  
            <input id="cc" 
                   class="easyui-combobox" 
                   name="user_group" 
                   data-options="valueField:'id',textField:'name',url:'<?php echo site_url('sistem/group/get_group_json'); ?>'" />  
        </div>
    </form>  
</div>  

<div id="tbuser" style="padding:3px">  
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()"></a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()"></a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()"></a>  
    <span>Username</span>  
    <input id="user_nama" style="border:1px solid #ccc">  
    <a href="#" class="easyui-linkbutton" plain="false" onclick="doSearch();">Search</a>  
</div>

<div id="dlg-buttons">  
    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">Save</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#user_dialog').dialog('close')">Cancel</a>  
</div>  