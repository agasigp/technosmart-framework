﻿<div class="SectionHeader">
	<h1 style="border:0px;text-align:center;"><?= $module[0]->mdl_deskripsi ?></h1>
</div>
<form id="searchForm">    
    <table class="list" width="100%">
        <tbody>
		  <tr class="blue_row">
            <td class="td_left_col">Judul <?= $module[0]->mdl_nama ?> </td>
            <td class="td_right_col"><input type="text" size="30" value="<?= $search ?>" name="name">
					<a class="actionbut" href="javascript:void(0);" onclick="confirmSubmit('<?= site_url($module[0]->mdl_link.$module[0]->mdl_id) ?>', 'searchForm', 'content')">Cari&nbsp;<img src="images/view.gif" /></a>
				</td>
        </tr>                
    </tbody>
	 </table>	 
</form>
<?php 
		if(trim($err) == '1')
			echo '<p class="dirmsg"><font class="success">Proses berhasil dilakukan</font></p>'; 
		if(trim($err) == '0') 
			echo '<p class="dirmsg"><font class="error">Proses gagal</font></p>';
		if(trim($err) == '')
			echo '';?>  
<div class="pagingdiv"> 
   <?= $paging['page']; ?>   
</div>

<form id='delForm'>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="list">
	<tbody>
   <tr class="listHead">
      <td class="left"><input type="checkbox" name="cek_data[]" id="cek_all" onclick="check_all(this.id, 'item_id_','<?php if(isset($paging['start'])) echo $paging['start']; else echo '0'; ?>','<?php if(isset($paging['start'])) echo ($paging['start'] + $paging['per_page']); else echo '0'; ?>');" /></td>      
		<td>NAMA</td>      
      <td>SETTING</td>            
      <td class="right">AKSI</td>		
	</tr>
   <?php  
         if(!empty($menu)):
			$no = $paging['start'];
			
         foreach ($menu as $item):
            ++$no;
            $tr_class = ($no % 2 == 0) ? "blue_row" : "white_row";            
				$td_class = ($no == ($paging['start'] + $paging['per_page']) || $no == $count) ? "tab_left_bot" : "td_left_border td_br_right";
            $td_action_class = ($no == ($paging['start'] + $paging['per_page']) || $no == $count) ? "tab_right_bot" : "td_br_right";
            ?>
            <tr class="<?= $tr_class ?>">
               <td class="<?= $td_class ?>"><input type="checkbox" name="item_id[]" value="<?= $item->setting_id ?>" id="item_id_<?= $no ?>" /></td>
               <td class="td_br_right"><?= $item->setting_name ?></td>
					<td class="td_br_right"><?= $item->setting_value ?></td>
               <td width="100px" class="<?= $td_action_class ?>">
                  <a href="javascript:void(0);" onclick="doActionLink('<?= site_url('/sistem/setting/upd/'.$module[0]->mdl_id.'/'.$item->setting_id) ?>', 'content', '')"><img src="images/edit.gif" /></a>
                  <a href="javascript:void(0);" onclick="doActionLink('<?= site_url('/sistem/setting/dodel/'.$module[0]->mdl_id.'/'.$item->setting_id) ?>', 'content', '', 'del')"><img src="images/hapus.gif" /></a>
				</td>
            </tr>
      <?php      
         endforeach;
			else:?>
				<tr class="white_row">
					<td class="tab_left_bot_noborder" />
					<td class="td_bottom_border" colspan="2">-- Data Tidak Ditemukan --</td>
					<td class="tab_right_bot" />
				</tr>
      <?php   
			endif;?>
      <tr class="listBot">
         <td colspan="5" class="left"></td>
         <td class="right"></td>
      </tr>
   </tbody>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="actionSec">
	<tbody>
      <tr>
    	<td style="padding-top: 6px;">
         <a class="actionbut" href="javascript:void(0);" onclick="scriptDoLoad('<?= site_url('/sistem/setting/add/'.$module[0]->mdl_id) ?>', 'content', '')">Tambah <?= $module[0]->mdl_nama ?>&nbsp;<img src="images/add.gif" /></a>
         <a class="actionbut" href="javascript:void(0);" onclick="confirmSubmit('<?= site_url('/sistem/setting/dodelmulti/'.$module[0]->mdl_id) ?>', 'delForm', 'content')">Hapus Terpilih&nbsp;<img src="images/hapus.gif" /></a>
    	</td>
	</tr>
   </tbody>
</table>
</form>