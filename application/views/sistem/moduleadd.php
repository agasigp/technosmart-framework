﻿<div class="SectionHeader">
	<h1 style="border:0px;text-align:center;"><?= $action." ".$module[0]->mdl_nama ?></h1>
</div>
<form id="<?php if(isset($moduleData)) $idForm = 'updmenu'; else $idForm = 'addmenu'; echo $idForm; ?>">
<input type="hidden" name="id" value='<?php if(isset($moduleData)) echo $moduleData[0]->mdl_id; ?>'>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="list">
	<tbody>
   <tr class="listHead">
		<td width="30%" class="left"><?= $action." ".$module[0]->mdl_nama ?></td>
		<td class="right">&nbsp;</td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Nama:</td>
		<td class="td_right_col"><input type="text" value="<?php if(isset($moduleData)) echo $moduleData[0]->mdl_nama; else echo set_value('name'); ?>" name="name">&nbsp;* 
         <?php echo form_error('name', '<font class="error">', '</font>'); ?></td>
	</tr>
   <tr class="blue_row">
		<td class="td_left_col">Deskripsi:</td>
		<td class="td_right_col"><input size="40" type="text" value="<?php if(isset($moduleData)) echo $moduleData[0]->mdl_deskripsi; else echo set_value('desc'); ?>" name="desc"></td>
	</tr>
   <tr class="white_row">
		<td class="td_left_col">Url:</td>
		<td class="td_right_col"><input size="40" type="text" value="<?php if(isset($moduleData)) echo $moduleData[0]->mdl_link; else echo set_value('url'); ?>" name="url">&nbsp;*
         <?php echo form_error('url', '<font class="error">', '</font>'); ?></td>
	</tr>
	<tr class="blue_row">
		<td class="td_left_col">Status aktif:</td>
		<td class="td_right_col">
         <input type="radio" name="status" value="Yes" <?php if(!isset($moduleData) || trim($moduleData[0]->mdl_is_aktif == 'Yes')) echo set_radio('status', 'Yes', TRUE); ?>>Aktif
         <input type="radio" name="status" value="No" <?php if(isset($moduleData) && trim($moduleData[0]->mdl_is_aktif == 'No')) echo set_radio('status', 'No', TRUE); ?>>Tidak Aktif
      &nbsp;*</td>
	</tr>	
   <tr class="white_row">
		<td class="td_left_col">Akses login</td>
		<td class="td_right_col">
         <input type="radio" name="login" value="Yes" <?php if(!isset($moduleData) || trim($moduleData[0]->mdl_need_login == 'Yes')) echo set_radio('login', 'Yes', TRUE); ?>>Ya
         <input type="radio" name="login" value="No" <?php if(isset($moduleData) && trim($moduleData[0]->mdl_need_login == 'No')) echo set_radio('login', 'No', TRUE); ?>>Tidak
      &nbsp;*</td>
	</tr>		
	<tr class="blue_row">
		<td class="tab_left_bot_noborder"></td>
		<td class="tab_right_bot"></td>
	</tr>
	
	<tr class="listBot">
		<td colspan="1" class="left"></td>
		<td class="right"></td>
	</tr>
</tbody>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="actionSec">
	<tbody><tr>
    	<td style="padding-top: 6px;text-align:right;">
    		<a class="actionbut" href="javascript:void(0);" onclick="scriptDoLoad('<?= site_url('/sistem/module/index/'.$module[0]->mdl_id) ?>', 'content', '')">Batal</a>&nbsp;
         <a class="actionbut" href="javascript:void(0);" onclick="confirmSubmit('<?php if(isset($moduleData)) $segment = 'upd/'.$module[0]->mdl_id.'/'.$moduleData[0]->mdl_id; else $segment = 'add/'.$module[0]->mdl_id;  
                                                                                 echo site_url('/sistem/module/'.$segment) ?>', '<?= $idForm ?>', 'content')">Simpan</a>
    	</td>
	</tr>
</tbody>
</table>
</form>