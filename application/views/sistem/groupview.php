<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Technosmart Framework</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?php echo base_url("css/bootstrap.css"); ?>" rel="stylesheet">
<!--        <link href="<?php echo base_url("css/bootstrap-dropdown-multilevel.css"); ?>" rel="stylesheet">-->
        <link href="<?php echo base_url("css/DT_bootstrap.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("css/tsfw.css"); ?>" rel="stylesheet">
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->

    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo site_url("tsfw"); ?>">TSFW</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="#">Home</a></li>
                            <?php
                            foreach ($menu as $menu_parent)
                            {
                                if ($menu_parent['parent_menu_id'] == 0) //jika menu parent = 0 (menu utama)
                                {
                                    echo "<li class=\"dropdown\">";
                                    echo "<a data-toggle=\"dropdown\" class=\"dropdown-toggle\">" . $menu_parent['menu_title'] . "<b class=\"caret\"></b></a>";
                                    echo "<ul class=\"dropdown-menu\">";
                                    foreach ($menu as $menu_child) //perulangan untuk mengetahui submenu
                                    {
                                        if ($menu_child['parent_menu_id'] == $menu_parent['menu_id']) //jika id submenu = id menu utama
                                        {
                                            $link = site_url($menu_child['mdl_link']);
                                            $menu_child_title = $menu_child['menu_title'];
                                            $menu_link = "<li><a href=\"$link\">";
                                            $menu_link .= $menu_child_title . "</a></li>";
                                            echo $menu_link;
                                        }
                                    }
                                    echo "</ul></li>";
                                }
                            }
                            ?>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container" id="content">
            <ul class="breadcrumb">
                <li><a href="#">Sistem</a><span class="divider">/</span></li>
                <li class="active">Group<span class="divider">/</span></li>
            </ul>

            <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-condensed table-striped datatable">
                <thead>
                    <tr>
                        <th>Group</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($group as $group_item) : ?>
                        <tr>
                            <td><?php echo $group_item['grp_nama'] ?></td>
                            <td><?php echo $group_item['grp_deskripsi'] ?></td>
                            <td>
                                <a class="btn btn-info btn-small" href="<?php echo site_url("sistem/group/update/".$group_item['grp_id']); ?>"><i class="icon-edit icon-white"></i> Edit</a>
                                <a class="btn btn-danger btn-small confirm" onclick="do_delete('<?php echo site_url("sistem/group/do_delete/".$group_item['grp_id']); ?>')" href="#"><i class="icon-edit icon-white"></i> Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->session->flashdata('message'); ?>
            <div class="form-actions">
                <a class="btn btn-primary" href="<?php echo site_url("sistem/group/add"); ?>">Tambah Data</a>
                <a class="btn btn-danger" onclick="javascript:history.go(-1)">Hapus Terpilih</a>
            </div>
        </div>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url("js/jquery-1.8.3.js"); ?>"></script>
        <script src="<?php echo base_url("js/bootstrap.js"); ?>"></script>
        <script src="<?php echo base_url("js/jquery.dataTables.js"); ?>"></script>
        <script src="<?php echo base_url("js/bootbox.js"); ?>"></script>
        <script src="<?php echo base_url("js/jquery.validate.js"); ?>"></script> 
        <script src="<?php echo base_url("js/tsfw.js"); ?>"></script> 
    </body>
</html>