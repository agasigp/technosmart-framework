﻿<div class="SectionHeader">
	<h1 style="border:0px;text-align:center;"><?= $action." ".$module[0]->mdl_nama ?></h1>
</div>   
<form id="<?php if(isset($setting)) $idForm = 'updmenu'; else $idForm = 'addmenu'; echo $idForm; ?>">
<input type="hidden" name="id" value='<?php if(isset($setting)) echo $setting[0]->setting_id; ?>'>
<input type="hidden" name="iconhidden" value='<?php if(isset($setting)) echo $setting[0]->menu_icon; ?>'>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="list">
	<tbody>
   <tr class="listHead">
		<td width="30%" class="left"><?= $action." ".$module[0]->mdl_nama ?></td>
		<td class="right">&nbsp;</td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Nama:</td>
		<td class="td_right_col"><input type="text" value="<?php if(isset($setting)) echo $setting[0]->setting_name; else echo set_value('name'); ?>" name="name">&nbsp;* 
         <?php echo form_error('name', '<font class="error">', '</font>'); ?></td>
	</tr>
   <tr class="blue_row">
		<td class="td_left_col">Nilai:</td>
		<td class="td_right_col"><input size="40" type="text" value="<?php if(isset($setting)) echo $setting[0]->setting_value; else echo set_value('nilai'); ?>" name="nilai">&nbsp;*
         <?php echo form_error('nilai', '<font class="error">', '</font>'); ?></td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Deskripsi:</td>
		<td class="td_right_col"><input type="text" size="40" value="<?php if(isset($setting)) echo $setting[0]->setting_description; else echo set_value('desc'); ?>" name="desc"></td>
	</tr>
	<tr class="blue_row">
		<td class="tab_left_bot_noborder"></td>
		<td class="tab_right_bot"></td>
	</tr>
	
	<tr class="listBot">
		<td colspan="1" class="left"></td>
		<td class="right"></td>
	</tr>
</tbody>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="actionSec">
	<tbody><tr>
    	<td style="padding-top: 6px;text-align:right;">
    		<a class="actionbut" href="javascript:void(0);" onclick="scriptDoLoad('<?= site_url('/sistem/setting/index/'.$module[0]->mdl_id) ?>', 'content', '')">Batal</a>&nbsp;
         <a class="actionbut" href="javascript:void(0);" onclick="confirmSubmit('<?php if(isset($setting)) $segment = 'upd/'.$module[0]->mdl_id.'/'.$setting[0]->setting_id; else $segment = 'add/'.$module[0]->mdl_id;  
                                                                                 echo site_url('/sistem/setting/'.$segment) ?>', '<?= $idForm ?>', 'content')">Simpan</a>
    	</td>
	</tr>
</tbody>
</table>
</form>