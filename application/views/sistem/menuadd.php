﻿<div class="SectionHeader">
	<h1 style="border:0px;text-align:center;"><?= $action." ".$module[0]->mdl_nama ?></h1>
</div>   

<form id="<?php if(isset($menu)) $idForm = 'updmenu'; else $idForm = 'addmenu'; echo $idForm; ?>" enctype="multipart/form-data" method="POST" action="<?php if(isset($menu)) $segment = 'upd/'.$module[0]->mdl_id.'/'.$menu[0]->menu_id; else $segment = 'add/'.$module[0]->mdl_id;echo site_url('/sistem/menu/'.$segment); ?>" target="hiddenFrame">
<input type="hidden" name="id" value='<?php if(isset($menu)) echo $menu[0]->menu_id; ?>'>
<input type="hidden" name="iconhidden" value='<?php if(isset($menu)) echo $menu[0]->menu_icon; ?>'>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="list">
	<tbody>
   <tr class="listHead">
		<td width="30%" class="left"><?= $action." ".$module[0]->mdl_nama ?></td>
		<td class="right">&nbsp;</td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Nama:</td>
		<td class="td_right_col"><input type="text" value="<?php if(isset($menu)) echo $menu[0]->menu_title; else echo set_value('title'); ?>" name="title">&nbsp;* 
         <?php echo form_error('title', '<font class="error">', '</font>'); ?></td>
	</tr>
   <tr class="blue_row">
		<td class="td_left_col">Judul:</td>
		<td class="td_right_col"><input size="40" type="text" value="<?php if(isset($menu)) echo $menu[0]->menu_display; else echo set_value('display'); ?>" name="display">&nbsp;*
         <?php echo form_error('display', '<font class="error">', '</font>'); ?></td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Status Aktif:</td>
		<td class="td_right_col">
         <input type="radio" name="status" value="Yes" <?php if(!isset($menu) || trim($menu[0]->menu_is_aktif == 'Yes')) echo set_radio('status', 'Yes', TRUE); ?>>Aktif
         <input type="radio" name="status" value="No" <?php if(isset($menu) && trim($menu[0]->menu_is_aktif == 'No')) echo set_radio('status', 'No', TRUE); ?>>Tidak Aktif
      &nbsp;*</td>
	</tr>	
	<tr class="blue_row">
		<td class="td_left_col">Modul:</td>
		<td class="td_right_col">
			<?php if(isset($menu)) $selected = $menu[0]->menu_mdl_id;
					else	$selected = '';
					echo combo_box('modul', $cmb_module, $selected); ?>&nbsp;*
         <?php echo form_error('modul', '<font class="error">', '</font>'); ?></td>
	</tr>	
	<tr class="white_row">
		<td class="td_left_col">Menu Induk:</td>
		<td class="td_right_col">
			<?php if(isset($menu)) $selected = $menu[0]->parent_menu_id;
					else	$selected = '';
					echo combo_box('parent', $menu_parent, $selected); ?></td>
	</tr>	
	<tr class="blue_row">
		<td class="td_left_col">No. Urut</td>
		<td class="td_right_col"><input type="text" size="5" value="<?php if(isset($menu)) echo $menu[0]->menu_urut; else echo set_value('no_urut'); ?>" name="no_urut">&nbsp;*
         <?php echo form_error('no_urut', '<font class="error">', '</font>'); ?></td>
	</tr>
	<tr class="white_row">
		<td class="td_left_col">Deskripsi:</td>
		<td class="td_right_col"><input type="text" size="40" value="<?php if(isset($menu)) echo $menu[0]->menu_deskripsi; else echo set_value('desc'); ?>" name="desc"></td>
	</tr>
	<tr class="blue_row">
		<td class="td_left_col">Icon:</td>
		<td class="td_right_col">
         <input type="file" name="icon" id="upload"><br />
         <?php if(isset($menu)) echo anchor(base_url().'uploads/'.$menu[0]->menu_icon, $menu[0]->menu_icon, 'title="File icon" target="_blank"'); ?>
      </td>
	</tr>	
	<tr class="white_row">
		<td class="tab_left_bot_noborder"></td>
		<td class="tab_right_bot"></td>
	</tr>
	
	<tr class="listBot">
		<td colspan="1" class="left"></td>
		<td class="right"></td>
	</tr>
</tbody>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="actionSec">
	<tbody><tr>
    	<td style="padding-top: 6px;text-align:right;">
    		<a class="actionbut" href="javascript:void(0);" onclick="scriptDoLoad('<?= site_url('/sistem/menu/index/'.$module[0]->mdl_id) ?>', 'content', '')">Batal</a>&nbsp;
         <!--<a class="actionbut" href="javascript:void(0);" onclick="confirmSubmit('<?php if(isset($menu)) $segment = 'upd/'.$module[0]->mdl_id.'/'.$menu[0]->menu_id; else $segment = 'add/'.$module[0]->mdl_id;  
                                                                                 echo site_url('/sistem/menu/'.$segment) ?>', '<?= $idForm ?>', 'content')">Simpan</a>
                                                                                 -->
         <input class="actionbut" type="submit" value="Simpan">
    	</td>
	</tr>
</tbody>
</table>
</form>
<iframe name="hiddenFrame" id="hiddenFrame" style="display:none;">