<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Technosmart Framework</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?php echo base_url("css/bootstrap.css"); ?>" rel="stylesheet">
<!--        <link href="<?php echo base_url("css/bootstrap-dropdown-multilevel.css"); ?>" rel="stylesheet">-->
        <link href="<?php echo base_url("css/DT_bootstrap.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("css/tsfw.css"); ?>" rel="stylesheet">
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->

    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo site_url("tsfw"); ?>">TSFW</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="#">Home</a></li>
                            <?php
                            foreach ($menu as $menu_parent)
                            {
                                if ($menu_parent['parent_menu_id'] == 0) //jika menu parent = 0 (menu utama)
                                {
                                    echo "<li class=\"dropdown\">";
                                    echo "<a data-toggle=\"dropdown\" class=\"dropdown-toggle\">" . $menu_parent['menu_title'] . "<b class=\"caret\"></b></a>";
                                    echo "<ul class=\"dropdown-menu\">";
                                    foreach ($menu as $menu_child) //perulangan untuk mengetahui submenu
                                    {
                                        if ($menu_child['parent_menu_id'] == $menu_parent['menu_id']) //jika id submenu = id menu utama
                                        {
                                            $link = site_url($menu_child['mdl_link']);
                                            $menu_child_title = $menu_child['menu_title'];
                                            $menu_link = "<li><a href=\"$link\">";
                                            $menu_link .= $menu_child_title . "</a></li>";
                                            echo $menu_link;
                                        }
                                    }
                                    echo "</ul></li>";
                                }
                            }
                            ?>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container" id="content">
            <ul class="breadcrumb">
                <li><a href="">Sistem</a><span class="divider">/</span></li>
                <li><a href="<?php echo site_url("sistem/group"); ?>">Group</a><span class="divider">/</span></li>
                <li class="active"><?= $action; ?><span class="divider">/</span></li>
            </ul>
            <?php
//            print_r($group);exit;
            echo validation_errors();
            $attribute = array('id' => 'group_form','class' => 'form-horizontal');
            if ($action === "Edit")
            {
                echo form_open(site_url("sistem/group/do_update"), $attribute);
                $hidden = (isset($group)? $group[0]->grp_id : "");
                echo form_hidden('group_id', $hidden);
            }
            else
            {
                echo form_open('sistem/group/do_add', $attribute);
            }
            ?>
            <fieldset>
                <legend><?= $action; ?> Group</legend>
            </fieldset>
            <div class="control-group">
                <label class="control-label" for="group_name">Nama</label>
                <div class="controls">
                    <input type="text" id="group_name" placeholder="Group" name="group_name"value="<?php if (isset($group)) echo $group[0]->grp_nama; else echo set_value('group_name'); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="deskripsi">Deskripsi</label>
                <div class="controls">
                    <input type="text" id="group_deskripsi" placeholder="Deskripsi" name="group_deskripsi" value="<?php if (isset($group)) echo $group[0]->grp_deskripsi; else echo set_value('group_deskripsi'); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="hak_akses">Menu</label>
                <div class="controls">
                    <?php
                    foreach ($akses_menu as $akses_menu_item):
                        $checked = '';
                        if (isset($hak_akses)):
                            foreach ($hak_akses as $hak_akses_item):
                                if (($akses_menu_item['menu_id'] == $hak_akses_item['menu_id']))
                                {
                                    $checked = 'checked';
                                    break;
                                }
                                else
                                {
                                    $checked = '';
                                }
                            endforeach;
                        endif;

                        if (trim($akses_menu_item['parent_menu_id']) == 0):
                            ?>
                            <b><?= $akses_menu_item['menu_title'] ?></b><br />
                        <?php else: ?>           
                            <label class="checkbox inline">
                                <?php
                                $data = array(
                                    'name' => 'hak_akses[]',
                                    'id' => 'hak_akses',
                                    'value' => $akses_menu_item['menu_id'],
                                    'checked' => $checked
                                );
                                echo form_checkbox($data);
                                echo $akses_menu_item['menu_title']
                                ?>
                            </label>
                        <?php
                        endif;
                    endforeach;
                    ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn" onclick="javascript:history.go(-1)">Cancel</button>
            </div>
            <?php
            echo form_close();
            ?>
        </div>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url("js/jquery-1.8.3.js"); ?>"></script>
        <script src="<?php echo base_url("js/bootstrap.js"); ?>"></script>
        <script src="<?php echo base_url("js/jquery.dataTables.js"); ?>"></script>
        <script src="<?php echo base_url("js/bootbox.js"); ?>"></script>
        <script src="<?php echo base_url("js/jquery.validate.js"); ?>"></script> 
        <script src="<?php echo base_url("js/tsfw.js"); ?>"></script> 
    </body>
</html>