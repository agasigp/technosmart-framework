<?php

class Setting_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
   function get_count_setting($search)
   {
      $search = '%'.$search.'%';
		$sql = "SELECT count(setting_id) as total
              FROM setting where setting_name like ?";            
      $query = $this->db->query($sql, array($search));        
      $result = $query->result();      
      return $result[0]->total;
   }
   
   function get_settings($search, $start, $limit)
   {
      $search = '%'.$search.'%';
		$sql = "SELECT 
               *
              FROM setting
				  where setting_name like ?
				  limit ?, ?";            
      $query = $this->db->query($sql, array($search, $start, $limit));
      $result = $query->result();
      return $result;      
   }
   
   function get_setting_by_id($id)
   {
      $sql = "SELECT 
               *
              FROM setting
              WHERE setting_id = ?";            
      $query = $this->db->query($sql, array($id));        
      $result = $query->result();
      return $result;      
   }
   
   function update_setting($data, $id)
   {
      $where = 'setting_id = ?';
      $str = $this->db->update_string('setting', $data, $where); 
      $query = $this->db->query($str, array($id));   
      return $query; 
   }
   
   function delete_setting_by_id($id)
   {
      $sql = "DELETE FROM setting WHERE setting_id = ?";            
      $query = $this->db->query($sql, array($id));
      return $query;      
   }
   
   function delete_setting_by_id_multi($id)
   {
      $sql = "DELETE FROM setting WHERE setting_id IN(?)";            
      $query = $this->db->query($sql, array($id));
      return $query;      
   }
   
   
   
   function add_setting($data)
   {
      $str = $this->db->insert_string('setting', $data); 
      $query = $this->db->query($str);
      return $query;
   }
   
}
?>
