<?php

class Unit_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
   function get_all_unit()
   {
      $sql = "SELECT 
               unit_id as id,
               unit_name as name
              FROM sys_unit";            
      $query = $this->db->query($sql);        
      $result = $query->result();
      return $result;      
   } 
   
   function get_count_unit($title)
   {
		$title = "%".$title."%";
		$sql = "select count(unit_id) as total from 
				sys_unit
				where unit_name like ?";
      $query = $this->db->query($sql, array($title));     
      $result = $query->result();
		return $result[0]->total;
	}
   
   function get_units($title, $start, $limit)
   {
		$title = "%".$title."%";
		$sql = "select 
               *
            from
				sys_unit				
				where unit_name like ?
				limit ?, ?";
      $query = $this->db->query($sql, array($title, $start, $limit));     
      return $query->result();
	}
   
   function get_unit_by_id($unitId)
   {
      $sql = "SELECT * FROM sys_unit where unit_id = ?";
      $query = $this->db->query($sql, array($unitId));     
      return $query->result();
   }
   
   function add_unit($data)
   {
      $str = $this->db->insert_string('sys_unit', $data); 
      $query = $this->db->query($str);
      return $query;
   }
   
   function update_unit($data, $id)
   {
      $where = 'unit_id = ?';
      $str = $this->db->update_string('sys_unit', $data, $where); 
      $query = $this->db->query($str, array($id));   
      return $query;      
   }
   
   function delete_unit_by_id($id)
   {
      $sql = 'DELETE FROM sys_unit WHERE unit_id = ?';
      $query = $this->db->query($sql, array($id));      
      return $query;      
   }
   
   function delete_unit_by_id_multi($id)
   {
      $sql = 'DELETE FROM sys_unit WHERE unit_id IN(?)';
      $query = $this->db->query($sql, array($id));      
      return $query;      
   }
   
   
}
?>
