<?php

class Menu_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_all_menu()
    {
        $sql = "select * from sys_menu ";
        $query = $this->db->query($sql, array());
        return $query->result_array();
    }

    function get_menu($groupId)
    {
        //query hirarkis mysql         
        $sql = "SELECT distinct CONCAT(REPEAT('', level - 1), 
               CAST(hi.menu_id AS CHAR)) AS menu_id, 
               parent_menu_id, 
               menu_title,
               mdl_link,
               mdl_id,
               level
            FROM    (
                    SELECT  hierarchy_connect_by_parent_eq_prior_id(menu_id) AS id, @level AS level
                    FROM    (
                            SELECT  
                     @start_with := 0,
                                    @id := @start_with,
                                    @level := 0
                            ) vars, sys_menu
                    WHERE   @id IS NOT NULL
                    ) ho
            JOIN    sys_menu hi ON hi.menu_id = ho.id
            join sys_group_menu on `menu_id` = grpmenu_menu_id
            join sys_group on grpmenu_grp_id = grp_id
            JOIN sys_module on mdl_id = menu_mdl_id
            where grp_id like ?";

        /* $sql = "select * from 
          sys_menu
          join sys_group_menu on `menu_id` = grpmenu_menu_id
          join sys_group on grpmenu_grp_id = grp_id
          JOIN sys_module on mdl_id = menu_mdl_id
          where grp_id = ?"; */
        $query = $this->db->query($sql, array($groupId));
        return $query->result_array();
    }

    function get_count_menu($title)
    {
        $title = "%" . $title . "%";
        $sql = "select count(b.menu_id) as total from 
				sys_menu a
				right join sys_menu b on a.menu_id = b.parent_menu_id 
				where b.menu_display like ?";
        $query = $this->db->query($sql, array($title));
        $result = $query->result();
        return $result[0]->total;
    }

    function get_menus($title, $start, $limit)
    {
        $title = "%" . $title . "%";
        $sql = "select 
					a.menu_display as parent, 
					b.menu_title, 
					b.menu_display, 
					b.menu_id from 
				sys_menu a
				right join sys_menu b on a.menu_id = b.parent_menu_id 
				where b.menu_display like ?
				order by a.menu_id asc limit ?, ?";
        $query = $this->db->query($sql, array($title, $start, $limit));
        return $query->result();
    }

    function get_menu_by_id($id)
    {
        $sql = "SELECT 
               *
              FROM sys_menu
              WHERE menu_id = ?";
        $query = $this->db->query($sql, array($id));
        $result = $query->result();
        return $result;
    }

    function get_all_parent_menu()
    {
        $sql = "select 					
					menu_title as name,
					menu_id as id
				from 
				sys_menu where parent_menu_id = 0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_parent_menu_by_menu_id($menu_id)
    {
        $sql = "select	distinct 
					parent_menu_id
				from 
				sys_menu where menu_id in (" . $this->db->escape_str($menu_id) . ")";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function add_menu($data)
    {
        $str = $this->db->insert_string('sys_menu', $data);
        $query = $this->db->query($str);
        return $query;
    }

    function update_menu($data, $id)
    {
        $where = 'menu_id = ?';
        $str = $this->db->update_string('sys_menu', $data, $where);
        $query = $this->db->query($str, array($id));
        return $query;
    }

    function delete_menu_by_id($id)
    {
        $sql = "DELETE FROM sys_menu WHERE menu_id = ?";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

    function delete_menu_by_id_multi($id)
    {
        $sql = "DELETE FROM sys_menu WHERE menu_id IN (?)";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

}
?>