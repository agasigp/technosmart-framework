<?php

class Group_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('sistem/menu_model'));
    }

    function get_count_group($search)
    {
        $search = '%' . $search . '%';
        $sql = "SELECT count(grp_id) as total
              FROM sys_group where grp_nama like ?";
        $query = $this->db->query($sql, array($search));
        $result = $query->result();
        return $result[0]->total;
    }

    function get_groups($search, $start, $limit)
    {
        $search = '%' . $search . '%';
        $sql = "SELECT 
               *
              FROM sys_group
				  where grp_nama like ?
				  limit ?, ?";
        $query = $this->db->query($sql, array($search, $start, $limit));
        $result = $query->result_array();
        return $result;
    }

    function get_all_groups()
    {
        $sql = "SELECT * FROM sys_group";
        $query = $this->db->query($sql, array());
        $result = $query->result_array();
        return $result;
    }

    function get_group_by_id($id)
    {
        $sql = "SELECT 
               *
              FROM sys_group
              WHERE grp_id = ?";
        $query = $this->db->query($sql, array($id));
        $result = $query->result();
        return $result;
    }

    function insert_menu_parent($groupId, $hak_akses)
    {
        $ImpHakAkses = implode(',', $hak_akses);
        $arrMenuParent = $this->menu_model->get_parent_menu_by_menu_id($ImpHakAkses);
        for ($i = 0; $i < count($arrMenuParent); $i++):
            $str = 'insert into sys_group_menu(grpmenu_grp_id, grpmenu_menu_id) values(?, ?)';
            $query = $this->db->query($str, array($groupId, $arrMenuParent[$i]->parent_menu_id));
        endfor;
        return $query;
    }

    function insert_menu($groupId, $hak_akses)
    {
        for ($i = 0; $i < count($hak_akses); $i++):
            $str = 'insert into sys_group_menu(grpmenu_grp_id, grpmenu_menu_id) values(?, ?)';
            $query = $this->db->query($str, array($groupId, $hak_akses[$i]));
        endfor;
        return $query;
    }

    function update_group($data, $id)
    {
        $where = 'grp_id = ?';
        //$str = $this->db->update_string('sys_group', $data, $where); 
        $this->db->trans_start();
        $str = 'update sys_group set grp_nama = ?, grp_deskripsi = ? where ' . $where;
        $this->db->query($str, array($data['grp_nama'], $data['grp_deskripsi'], $id));
        $this->delete_sys_group_menu_by_grp_id($id);

        if (isset($data['hak_akses'])):
            //insert menu parent
            $this->insert_menu_parent($id, $data['hak_akses']);
            //insert menu child
            $this->insert_menu($id, $data['hak_akses']);
        endif;
        $query = $this->db->trans_complete();
        return $query;
    }

    function delete_sys_group_menu_by_grp_id($id)
    {
        $sql = "DELETE FROM sys_group_menu WHERE grpmenu_grp_id = ?";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

    function delete_group_by_id($id)
    {
        $sql = "DELETE FROM sys_group WHERE grp_id = ?";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

    function delete_group_by_id_multi($id)
    {
        $sql = "DELETE FROM sys_group WHERE grp_id IN (?)";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

    function add_group($data)
    {
        $this->db->trans_start();
        //$str = $this->db->insert_string('sys_group', $data); 
        $str = 'insert into sys_group(grp_nama, grp_deskripsi) values(?, ?)';
        $this->db->query($str, array($data['grp_nama'], $data['grp_deskripsi']));
        $last_id = $this->db->insert_id();
        if (isset($data['hak_akses'])):
            /* for($i = 0; $i < count($data['hak_akses']); $i++):
              $str = 'insert into sys_group_menu(grpmenu_grp_id, grpmenu_menu_id) values(?, ?)';
              $this->db->query($str, array($last_id, $data['hak_akses'][$i]));
              endfor; */
            //insert menu parent
            $this->insert_menu_parent($last_id, $data['hak_akses']);

            //insert menu child
            $this->insert_menu($last_id, $data['hak_akses']);
        endif;
        $query = $this->db->trans_complete();
        return $query;
    }

}
?>