<?php

class Module_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
   function get_module_by_id($moduleId)
   {
      $sql = "SELECT * FROM sys_module where mdl_id = ?";
      $query = $this->db->query($sql, array($moduleId));     
      return $query->result();
   }
	
	function get_all_module()
   {
      $sql = "SELECT 
               mdl_id as id,
               mdl_nama as name
              FROM sys_module";            
      $query = $this->db->query($sql);        
      $result = $query->result();
      return $result;      
   } 
   
   function get_count_module($title)
   {
		$title = "%".$title."%";
		$sql = "select count(mdl_id) as total from 
				sys_module
				where mdl_nama like ?";
      $query = $this->db->query($sql, array($title));     
      $result = $query->result();
		return $result[0]->total;
	}
   
   function get_modules($title, $start, $limit)
   {
		$title = "%".$title."%";
		$sql = "select 
               mdl_id,
					mdl_nama, 
               mdl_deskripsi,
               mdl_is_aktif
            from
				sys_module				
				where mdl_nama like ?
				limit ?, ?";
      $query = $this->db->query($sql, array($title, $start, $limit));     
      return $query->result();
	}
   
   
   function add_module($data)
   {
      $str = $this->db->insert_string('sys_module', $data); 
      $query = $this->db->query($str);
      return $query;
   }
   
   function update_module($data, $id)
   {
      $where = 'mdl_id = ?';
      $str = $this->db->update_string('sys_module', $data, $where); 
      $query = $this->db->query($str, array($id));   
      return $query;      
   }
   
   function delete_module_by_id($id)
   {
      $sql = "DELETE FROM sys_module WHERE mdl_id = ?";            
      $query = $this->db->query($sql, array($id));      
      return $query;      
   }
   
   function delete_module_by_id_multi($id)
   {
      $sql = "DELETE FROM sys_module WHERE mdl_id IN(?)";            
      $query = $this->db->query($sql, array($id));      
      return $query;      
   }
   
   
}
?>
