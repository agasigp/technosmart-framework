<?php

class User_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
    function get_count_users($search)
    {
        $search = '%'.$search.'%';
        $sql = "SELECT count(user_id) as total
        FROM sys_user where user_nama like ?";            
        $query = $this->db->query($sql, array($search));        
        $result = $query->result();      
        return $result[0]->total;
    }
    function get_all_users($start, $limit) 
    {
//        $search = '%'.$search.'%';
        $sql = "SELECT 
                user_id,
                user_nama,
                user_real_nama,
                user_email,
                user_status 
                FROM sys_user             
                LIMIT ?, ?";
        $query = $this->db->query($sql, array($start, $limit));
        $result = $query->result_array();
        return $result;
    }
    function get_users($search, $start, $limit)
    {
        $search = '%'.$search.'%';
        $sql = "SELECT 
        user_id,
        user_nama,
        user_real_nama,
        user_email,
        user_status
        FROM sys_user 
        WHERE user_nama LIKE ?
        LIMIT ?, ?";            
        $query = $this->db->query($sql, array($search, $start, $limit));
        $result = $query->result_array();
        return $result;      
    }

    function get_user_by_id($userId)
    {
        $sql = "SELECT 
        *
        FROM sys_user
        WHERE user_id = ?";            
        $query = $this->db->query($sql, array($userId));        
        $result = $query->result();
        return $result;      
    }

    function get_user_by_name_password($userName, $password)
    {
        $sql = "SELECT 
        user_nama,
        user_grp_id,
        grp_nama
        FROM sys_user
        join sys_group on grp_id = user_grp_id
        WHERE user_nama = ? and user_password = ?";            
        $query = $this->db->query($sql, array($userName, $password));        
        $result = $query->result();
        return $result;      
    }

    function update_user($data, $userId)
    {
        $where = 'user_id = ?';
        $str = $this->db->update_string('sys_user', $data, $where); 
        $query = $this->db->query($str, array($userId));   
        return $query;      
    }

    function delete_user_by_id($userId)
    {
        $sql = "DELETE FROM sys_user WHERE user_id = ?";            
        $query = $this->db->query($sql, array($userId));      
        return $query;      
    }

    function delete_user_by_id_multi($userId)
    {
        $sql = "DELETE FROM sys_user WHERE user_id IN (?)";            
        $query = $this->db->query($sql, array($userId));      
        return $query;      
    }
    
    function add_user($data)
    {
        $str = $this->db->insert_string('sys_user', $data); 
        $query = $this->db->query($str);
        return $query;
    }
}
?>