<?php

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistem/user_model');
    }

    public function index()
    {
        $this->load->view('login/login.php');
    }

    public function do_login()
    {
        $arrPost = $this->input->post();
        $data['login'] = $this->user_model->get_user_by_name_password($arrPost['username'], md5($arrPost['password']));
//        print_r($arrPost);
//        echo count($data['login']);
//        exit;
        if (count($data['login']) > 0)
        {
            $session = array(
                'username' => $data['login'][0]->user_nama,
                'groupid' => $data['login'][0]->user_grp_id,
                'unitid' => $data['login'][0]->user_unit_id,
                'groupnama' => $data['login'][0]->grp_nama,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($session);
            redirect(base_url(), 'location');
        }
        else
        {
            $this->session->set_flashdata('eror', 'Username/password salah');
            redirect(site_url('login/login'), 'location');
        }
    }

    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect(site_url('/login/login/'), 'location');
    }

}
?>