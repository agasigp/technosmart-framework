<?php
class Tsfw extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sistem/menu_model');
//        $this->output->enable_profiler(TRUE);
    }
    
    public function index()
    {
        if ($this->session->userdata('username') && $this->session->userdata('logged_in'))
        {
            $group_id = $this->session->userdata('groupid');
            $data['menu'] = $this->menu_model->get_menu($group_id);
            $this->load->view('tsfw.php',$data);           
        } 
        else
        {
            redirect('/login/login','location');
        }
    }
}
?>