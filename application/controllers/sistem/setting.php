<?php
class Setting extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
      $this->load->library(array('pagination', 'form_validation', 'session'));      
      $this->load->helper('form');      
      $this->load->model('sistem/setting_model');       

		$CI =& get_instance();
		$moduleId = $CI->uri->segment(4);		
		$this->module = $this->module_model->get_module_by_id($moduleId);
	}
	
	public function index()
	{		
		$data['module'] = $this->module;
      $arrPost = $this->input->post();		
		
		$data['count'] = $this->setting_model->get_count_setting($arrPost['name']);		
		$data['search'] = $arrPost['name'];
			
      $base_url = site_url($data['module'][0]->mdl_link.$data['module'][0]->mdl_id);
      $data['paging'] = paging($this->pagination, $data['count'], $base_url);		
      $data['menu'] = $this->setting_model->get_settings($arrPost['name'], $data['paging']['start'], (int)$data['paging']['per_page']);    
		
		
      $data['err'] = $this->session->flashdata('err');
		$this->load->view('sistem/settingview', $data);		
	}   
   
   public function upd($moduleId, $settingId)
	{			
		$data['module'] = $this->module;
		$data['setting'] = $this->setting_model->get_setting_by_id($settingId);  
      $data['action'] = 'Edit';      
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         $arrPost = $this->input->post();
         //do udpate   
         $data = array('setting_name' => $arrPost['name'], 'setting_value' => $arrPost['nilai'], 'setting_description' => $arrPost['desc']);
         $query = $this->setting_model->update_setting($data, $arrPost['id']);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }
      
      $this->load->view('sistem/settingadd', $data);
	}
   
   public function validate_input(){
      //form validation      
      $this->form_validation->set_message('required', '%s harus diisi.');      
      
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');      
		$this->form_validation->set_rules('nilai', 'Nilai', 'trim|required');      
   }
   
   public function dodel($moduleId, $settingId)
   {
         //do delete
         $query = $this->setting_model->delete_setting_by_id($settingId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
	}
   
   public function dodelmulti($moduleId)
   {
         $arrPost = $this->input->post();
         $arrId = implode(',', $arrPost['item_id']);
         $query = $this->setting_model->delete_setting_by_id_multi($arrId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
   }
   
   public function add()
	{			
		$data['module'] = $this->module;
		$data['action'] = 'Tambah';
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         //do insert 			
         $arrPost = $this->input->post();         
			$data = array('setting_name' => $arrPost['name'], 'setting_value' => $arrPost['nilai'], 'setting_description' => $arrPost['desc']);
         $query = $this->setting_model->add_setting($data);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }      
      $this->load->view('sistem/settingadd', $data);
	}
}
?>
