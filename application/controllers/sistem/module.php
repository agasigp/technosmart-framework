<?php
class Module extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
      $this->load->library(array('pagination', 'form_validation'));      
      $this->load->helper('form');      
      $this->load->model('sistem/module_model'); 

		$CI =& get_instance();
		$moduleId = $CI->uri->segment(4);		
		$this->module = $this->module_model->get_module_by_id($moduleId);
	}
	
	public function index()
	{		
		$data['module'] = $this->module;
      $arrPost = $this->input->post();		
		
		$data['count'] = $this->module_model->get_count_module($arrPost['name']);		
		$data['search'] = $arrPost['name'];
			
      $base_url = site_url($data['module'][0]->mdl_link.$data['module'][0]->mdl_id);
      $data['paging'] = paging($this->pagination, $data['count'], $base_url);		
      $data['moduledata'] = $this->module_model->get_modules($arrPost['name'], $data['paging']['start'], (int)$data['paging']['per_page']);    
		
		
      $data['err'] = $this->session->flashdata('err');
		$this->load->view('sistem/moduleview', $data);		
	}   
   
   public function upd($moduleId, $moduleDataId)
	{			
		$data['module'] = $this->module;
		$data['moduleData'] = $this->module_model->get_module_by_id($moduleDataId);        
      $data['action'] = 'Edit';      
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         $arrPost = $this->input->post();
         //do udpate   
         $data = array('mdl_nama' => $arrPost['name'], 'mdl_is_aktif' => $arrPost['status'], 'mdl_need_login' => $arrPost['login'],
                      'mdl_deskripsi' => $arrPost['desc'], 'mdl_link' => $arrPost['url']);         
         $query = $this->module_model->update_module($data, $arrPost['id']);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }
      
      $this->load->view('sistem/moduleadd', $data);
	}
   
   public function validate_input(){
      //form validation      
      $this->form_validation->set_message('required', '%s harus diisi.');      
      
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');      
		$this->form_validation->set_rules('url', 'Url', 'trim|required');      
		$this->form_validation->set_rules('status', 'Status aktif', 'trim|required');      
		$this->form_validation->set_rules('login', 'Akses login', 'trim|required');
		
   }
   
   public function dodel($moduleId, $moduleId)
   {
         //do delete
         $query = $this->module_model->delete_module_by_id($moduleId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
	}
   
   public function dodelmulti($moduleId)
   {
         $arrPost = $this->input->post();
         $arrId = implode(',', $arrPost['item_id']);
         $query = $this->module_model->delete_module_by_id_multi($arrId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
   }
   
   public function add()
	{			
		$data['module'] = $this->module;
		$data['action'] = 'Tambah';		
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         //do insert 			
         $arrPost = $this->input->post();         
			$data = array('mdl_nama' => $arrPost['name'], 'mdl_is_aktif' => $arrPost['status'], 'mdl_need_login' => $arrPost['login'],
                      'mdl_deskripsi' => $arrPost['desc'], 'mdl_link' => $arrPost['url']);
         $query = $this->module_model->add_module($data);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }      
      $this->load->view('sistem/moduleadd', $data);
	}	
	
}
?>
