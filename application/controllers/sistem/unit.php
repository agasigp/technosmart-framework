<?php
class Unit extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
      $this->load->library(array('pagination', 'form_validation', 'session'));      
      $this->load->helper('form');      
      $this->load->model('sistem/unit_model');       

		$CI =& get_instance();
		$moduleId = $CI->uri->segment(4);		
		$this->module = $this->module_model->get_module_by_id($moduleId);
	}
	
	public function index()
	{		
		$data['module'] = $this->module;
      $arrPost = $this->input->post();		
		
		$data['count'] = $this->unit_model->get_count_unit($arrPost['name']);		
		$data['search'] = $arrPost['name'];
			
      $base_url = site_url($data['module'][0]->mdl_link.$data['module'][0]->mdl_id);
      $data['paging'] = paging($this->pagination, $data['count'], $base_url);		
      $data['menu'] = $this->unit_model->get_units($arrPost['name'], $data['paging']['start'], (int)$data['paging']['per_page']);    
		
		
      $data['err'] = $this->session->flashdata('err');
		$this->load->view('sistem/unitview', $data);		
	}   
   
   public function upd($moduleId, $unitId)
	{			
		$data['module'] = $this->module;
		$data['unit'] = $this->unit_model->get_unit_by_id($unitId);  
      $data['action'] = 'Edit';      
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         $arrPost = $this->input->post();
         //do udpate   
         $data = array('unit_name' => $arrPost['name']);
         $query = $this->unit_model->update_unit($data, $arrPost['id']);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }
      
      $this->load->view('sistem/unitadd', $data);
	}
   
   public function validate_input(){
      //form validation      
      $this->form_validation->set_message('required', '%s harus diisi.'); 
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');      
   }
   
   public function dodel($moduleId, $unitId)
   {
         //do delete
         $query = $this->unit_model->delete_unit_by_id($unitId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
	}
   
   public function dodelmulti($moduleId)
   {
         $arrPost = $this->input->post();
         $arrId = implode(',', $arrPost['item_id']);
         $query = $this->unit_model->delete_unit_by_id_multi($arrId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
   }
   
   public function add()
	{			
		$data['module'] = $this->module;
		$data['action'] = 'Tambah';
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         //do insert 			
         $arrPost = $this->input->post();         
			$data = array('unit_name' => $arrPost['name']);
         $query = $this->unit_model->add_unit($data);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }      
      $this->load->view('sistem/unitadd', $data);
	}
}
?>
