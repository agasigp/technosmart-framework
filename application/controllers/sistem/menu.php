<?php
class Menu extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
      $this->load->library(array('pagination', 'form_validation', 'session'));      
      $this->load->helper('form');      
      $this->load->model('sistem/menu_model');       

		$CI =& get_instance();
		$moduleId = $CI->uri->segment(4);		
		$this->module = $this->module_model->get_module_by_id($moduleId);
	}
	
	public function index()
	{		
		$data['module'] = $this->module;
      $arrPost = $this->input->post();		
		
		$data['count'] = $this->menu_model->get_count_menu($arrPost['name']);		
		$data['search'] = $arrPost['name'];
			
      $base_url = site_url($data['module'][0]->mdl_link.$data['module'][0]->mdl_id);
      $data['paging'] = paging($this->pagination, $data['count'], $base_url);		
      $data['menu'] = $this->menu_model->get_menus($arrPost['name'], $data['paging']['start'], (int)$data['paging']['per_page']);
		
      $data['err'] = $this->session->flashdata('err');      
      $data['fajax'] = ($this->session->flashdata('fajax') == '1') ? "parent.document.getElementById('content').innerHTML = document.body.innerHTML" : "";
      
		$this->load->view('sistem/menuview', $data);		
	}   
   
   public function upd($moduleId, $menuId)
	{			
		$data['module'] = $this->module;
		$data['menu'] = $this->menu_model->get_menu_by_id($menuId);  
      $data['cmb_module'] = $this->module_model->get_all_module();		
		$data['menu_parent'] = $this->menu_model->get_all_parent_menu();
      $data['action'] = 'Edit';      
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         $upload_path = './uploads/';
         move_uploaded_file($_FILES['icon']['tmp_name'], $upload_path.$_FILES['icon']['name']);
         
         $arrPost = $this->input->post();
         //do udpate   
         $icon = (trim($_FILES['icon']['name']) == '')  ? $arrPost['iconhidden'] : $_FILES['icon']['name'];
         $data = array('menu_title' => $arrPost['title'], 'menu_is_aktif' => $arrPost['status'], 'menu_mdl_id' => $arrPost['modul'], 
                        'parent_menu_id' => $arrPost['parent'], 'menu_urut' => $arrPost['no_urut'], 'menu_display' => $arrPost['display'], 
                        'menu_deskripsi' => $arrPost['desc'], 'menu_icon' => $icon);
         $query = $this->menu_model->update_menu($data, $arrPost['id']);
         $this->session->set_flashdata('err', $query);
         $this->session->set_flashdata('fajax', 1);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');         
      }
      
      $this->load->view('sistem/menuadd', $data);
	}
   
   public function validate_input(){
      //form validation      
      $this->form_validation->set_message('required', '%s harus diisi.');      
      
		$this->form_validation->set_rules('title', 'Nama', 'trim|required');      
		$this->form_validation->set_rules('display', 'Judul', 'trim|required');      
		$this->form_validation->set_rules('status', 'Status aktif', 'trim|required');      
		$this->form_validation->set_rules('modul', 'Modul', 'callback_combo_check');
		$this->form_validation->set_rules('no_urut', 'No urut', 'trim|required');
   }
   
   public function dodel($moduleId, $menuId)
   {
         //do delete
         $query = $this->menu_model->delete_menu_by_id($menuId);   
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
	}
   
   public function dodelmulti($moduleId)
   {
         $arrPost = $this->input->post();
         $arrId = implode(',', $arrPost['item_id']);
         $query = $this->menu_model->delete_menu_by_id_multi($arrId);   
         $this->session->set_flashdata('err', $query);
         redirect('/sistem/menu/index/'.$moduleId.'/', 'location');
   }
   
   public function add()
	{			
		$data['module'] = $this->module;
		$data['action'] = 'Tambah';
		$data['cmb_module'] = $this->module_model->get_all_module();		
		$data['menu_parent'] = $this->menu_model->get_all_parent_menu();
		
      call_user_func_array(array($this, 'validate_input'), '');
      
      if ($this->form_validation->run() == FALSE){
         
      }else{
         //do insert 
         $upload_path = './uploads/';
         move_uploaded_file($_FILES['icon']['tmp_name'], $upload_path.$_FILES['icon']['name']);         
			
         $arrPost = $this->input->post();         
         $data = array('menu_title' => $arrPost['title'], 'menu_is_aktif' => $arrPost['status'], 'menu_mdl_id' => $arrPost['modul'], 
                        'parent_menu_id' => $arrPost['parent'], 'menu_urut' => $arrPost['no_urut'], 'menu_display' => $arrPost['display'], 
                        'menu_deskripsi' => $arrPost['desc'], 'menu_icon' => $arrPost['icon']);
         $query = $this->menu_model->add_menu($data);
         $this->session->set_flashdata('err', $query);
         redirect($this->module[0]->mdl_link.$this->module[0]->mdl_id, 'location');
      }      
      $this->load->view('sistem/menuadd', $data);
	}
	
	function combo_check($str)
	{	      
      
		if ($str == '0'){         
			$this->form_validation->set_message('combo_check', 'Kotak isian harus diisi salah satu nilai');
			return FALSE;
		}
		else{         
			return TRUE;
		}
	} 
}
?>
