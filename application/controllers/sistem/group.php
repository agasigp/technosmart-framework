<?php

class Group extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper(array('form'));
        $this->load->model(array('sistem/menu_model', 'sistem/group_model'));
    }

    public function index()
    {
        $group_id = $this->session->userdata('groupid');
        $data['menu'] = $this->menu_model->get_menu($group_id);
        $data['group'] = $this->group_model->get_all_groups();
        $this->load->view('sistem/groupview', $data);
    }

    public function update($groupId)
    {
        $group_id = $this->session->userdata('groupid');
        $data['menu'] = $this->menu_model->get_menu($group_id);
        $data['akses_menu'] = $this->menu_model->get_all_menu();
        $data['hak_akses'] = $this->menu_model->get_menu($groupId);

        $data['group'] = $this->group_model->get_group_by_id($groupId);
        $data['action'] = 'Edit';
        $this->load->view('sistem/groupadd', $data);
    }

    public function do_update()
    {
        if ($this->form_validation->run() == FALSE)
        {
            
        }
        else
        {
            
        }
        $arrPost = $this->input->post();
        //do udpate         
        if (isset($arrPost['hak_akses']))
            $data = array('grp_nama' => $arrPost['group_name'], 'grp_deskripsi' => $arrPost['group_deskripsi'], 'hak_akses' => $arrPost['hak_akses']);
        else
            $data = array('grp_nama' => $arrPost['group_name'], 'grp_deskripsi' => $arrPost['group_deskripsi']);

//        print_r($arrPost);
//        exit;
        $query = $this->group_model->update_group($data, $arrPost['group_id']);
        $this->session->set_flashdata('message', '<div class="alert alert-success">Sukses update data</div>');
        redirect(site_url("sistem/group"), 'location');
    }

    public function validate_input()
    {
        //form validation      
        $this->form_validation->set_message('required', '%s harus diisi.');
        $this->form_validation->set_rules('groupName', 'Nama grup', 'trim|required');
    }

    public function do_delete($groupId)
    {
        //do delete
        $query = $this->group_model->delete_group_by_id($groupId);
        $this->session->set_flashdata('err', $query);
        redirect($this->module[0]->mdl_link . $this->module[0]->mdl_id, 'location');
    }

    public function dodelmulti($moduleId)
    {
        $arrPost = $this->input->post();
        $arrId = implode(',', $arrPost['item_id']);
        $query = $this->group_model->delete_group_by_id_multi($arrId);
        $this->session->set_flashdata('err', $query);
        redirect('/sistem/group/index/' . $moduleId . '/', 'location');
    }

    public function add()
    {
        $group_id = $this->session->userdata('groupid');
        $data['menu'] = $this->menu_model->get_menu($group_id);
        $data['akses_menu'] = $this->menu_model->get_all_menu();
        $data['action'] = 'Tambah';

        $this->load->view('sistem/groupadd', $data);
    }

    public function do_add()
    {
        if ($this->form_validation->run() == FALSE)
        {
            
        }
        else
        {
            
        }
        $arrPost = $this->input->post();
        if (isset($arrPost['hak_akses']))
            $data = array('grp_nama' => $arrPost['group_name'], 'grp_deskripsi' => $arrPost['group_deskripsi'], 'hak_akses' => $arrPost['hak_akses']);
        else
            $data = array('grp_nama' => $arrPost['group_name'], 'grp_deskripsi' => $arrPost['group_deskripsi']);

        $query = $this->group_model->add_group($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success">Sukses tambah data</div>');
        redirect(site_url("sistem/group"), 'location');
    }

    public function get_group_json()
    {
        $group = $this->group_model->get_all_groups();
        $this->output->set_content_type('application/json')->set_output(json_encode($group));
    }

}
?>