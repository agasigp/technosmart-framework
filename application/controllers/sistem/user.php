<?php

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('form');
        $this->load->model(array('sistem/user_model', 'sistem/unit_model', 'sistem/group_model'));
    }

    public function index()
    {
        $this->load->view('sistem/userview');
    }

    public function get_user_json()
    {
        $arrPost = $this->input->post();
        $page = isset($arrPost['page']) ? intval($arrPost['page']) : 1;
        $rows = isset($arrPost['rows']) ? intval($arrPost['rows']) : 10;
        $search = isset($arrPost['user_nama']) ? mysql_real_escape_string($arrPost['user_nama']) : '';
        $offset = ($page - 1) * $rows;

        $result = array();
        $row = $this->user_model->get_count_users($search);
        $result['total'] = $row[0];

        $items = array();
        $rs = $this->user_model->get_users($search, $offset, $rows);
        foreach ($rs as $rslt)
        {
            array_push($items, $rslt);
        }
        $result['rows'] = $items;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
//        print_r($arrPost);
    }

    public function upd($userId)
    {
        $data['user'] = $this->user_model->get_user_by_id($userId);
//        $data['group'] = $this->group_model->get_all_groups();
//        $data['unit'] = $this->unit_model->get_all_unit();
//        $data['action'] = 'Edit';      
//        call_user_func_array(array($this, 'validate_input'), '');

        if ($this->form_validation->run() == FALSE)
        {
            
        }
        else
        {
            $arrPost = $this->input->post();
            //do udpate         
            $data = array('user_nama' => $arrPost['userName'], 'user_real_nama' => $arrPost['name'],
                'user_email' => $arrPost['email'], 'user_unit_id' => $arrPost['unit'],
                'user_status' => $arrPost['status'], 'user_grp_id' => $arrPost['group']);
            $query = $this->user_model->update_user($data, $arrPost['id']);
            $this->session->set_flashdata('err', $query);
            redirect('/sistem/user/index/2/', 'location');
        }

        $this->load->view('sistem/useradd', $data);
    }

    public function validate_input()
    {
        //form validation      
        $this->form_validation->set_message('required', '%s harus diisi.');
        $this->form_validation->set_message('matches', '%s tidak sesuai dengan %s');
        $this->form_validation->set_message('email', 'Email tidak valid.');

        $this->form_validation->set_rules('userName', 'Nama', 'trim|required');

        $this->form_validation->set_rules('name', 'Nama lengkap', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $this->form_validation->set_rules('unit', 'Sekolah', 'callback_unit_check');
        $this->form_validation->set_rules('group', 'Sekolah', 'callback_unit_check');
    }

    public function dodel($userId)
    {
        //do delete
        $query = $this->user_model->delete_user_by_id($userId);
//        $this->session->set_flashdata('err', $query);
//        redirect('/sistem/user/index/2/', 'location');
    }

    public function dodelmulti($moduleId)
    {
        $arrPost = $this->input->post();
        $arrId = implode(',', $arrPost['item_id']);
        $query = $this->user_model->delete_user_by_id_multi($arrId);
        $this->session->set_flashdata('err', $query);
        redirect('/sistem/user/index/' . $moduleId . '/', 'location');
    }

    public function add()
    {
//        $data['action'] = 'Tambah';
//        $data['unit'] = $this->unit_model->get_all_unit();
//        $data['group'] = $this->group_model->get_all_groups();
//
//        call_user_func_array(array($this, 'validate_input'), '');
//
//        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirmPassword]|md5');
//        $this->form_validation->set_rules('confirmPassword', 'Konfirmasi Password', 'trim|required');
//
//        if ($this->form_validation->run() == FALSE){
//
//        }
//        else
//        {
        $arrPost = $this->input->post();
        $data = array(
            'user_nama' => $arrPost['user_nama'],
            'user_password' => $arrPost['password'],
            'user_real_nama' => $arrPost['user_real_nama'],
            'user_email' => $arrPost['user_email'],
            'user_status' => $arrPost['user_status'],
            'user_grp_id' => $arrPost['user_group']);
        
        $query = $this->user_model->add_user($data);
//        $this->session->set_flashdata('err', $query);
//        redirect('/sistem/user/index/2/', 'location');
//        }      
//        $this->load->view('sistem/useradd', $data);
    }

    function unit_check($str)
    {
        if ($str == '0')
        {
            $this->form_validation->set_message('unit_check', 'Combobox harus dipilih ');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

}
?>